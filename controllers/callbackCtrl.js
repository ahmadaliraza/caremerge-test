//Callbacks and anonymous functions

const request = require('../request-client')

function parseTitle(url, callback) {

  request.get(url, function (err, response) {

    if (err) {
      return callback(err)
    }

    //error-first callbacks

    callback(null, response.body)

  })

}
function parseTitles(addresses, results, index, callback) {

  if (index === addresses.length) {
    return callback(null, results)
  }

  const url = addresses[index];

  parseTitle(url, function (err, data) {

    if (err) {
      
      results.push({ url, error: err.code })

    } else {

      results.push({ url, title: data.match(/<title[^>]*>([^<]+)<\/title>/)[1] })
      
    }

    index += 1;

    parseTitles(addresses, results, index, callback)

  })

}


exports.getTile = function getTile(req, res) {

  const addresses = typeof req.query.address === 'string' ? [req.query.address] : req.query.address;

  parseTitles(addresses, [], 0, function (err, results) {

    if (err) {

      // handle error 
      results = addresses.map((addr) => { return { url: addr, error: 'something went wrong' } })

    }

    res.render('index.ejs', { results })

  })



}