module.exports = {
  asyncCtrl: require('./asyncCtrl'),
  callbackCtrl: require('./callbackCtrl'),
  promiseCtrl: require('./promiseCtrl')
}