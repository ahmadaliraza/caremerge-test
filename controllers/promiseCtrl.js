// using Promises and Arrow functions and Exports

const request = require('../request-client')

const parseTitles = (addresses) => {

  // not going to reject promise for any case
  return new Promise((resolve, reject) => {

    const results = []


    //using Array Reduce instead of Promise.all because we wanted to complete the loop even if any request throws error

    const prom = addresses.reduce((prev, url) => {

      return prev.then(() => {

        return request.getWithPromise(url).then((response) => {

          results.push({ url, title: response.match(/<title[^>]*>([^<]+)<\/title>/)[1] })

        }).catch(err => {

          results.push({ url, error: err.error ? err.error.code : (err.cause ? err.cause.code : err.message) })

        })

      })

    }, Promise.resolve());


    prom.then(() => resolve(results)).catch(() => resolve(results))

  })

}


exports.getTile = (req, res) => {

  const addresses = typeof req.query.address === 'string' ? [req.query.address] : req.query.address;

  parseTitles(addresses).then(results => {

    res.render('index.ejs', { results })

  }).catch((err) => {

    res.render('index.ejs', {

      results: addresses.map((addr) => { return { url: addr, error: 'something went wrong' } })

    })


  })

}