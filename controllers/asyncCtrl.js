// implemented ES6 classes and async/await 


const request = require('../request-client')

class AsyncCtrl {

  static async  getTile(req, res) {

    try {

      const addresses = typeof req.query.address === 'string' ? [req.query.address] : req.query.address;

      const results = await AsyncCtrl.parseTitles(addresses)

      res.render('index.ejs', { results })

    } catch (err) {

      res.render('index.ejs', {

        results: addresses.map((addr) => { return { url: addr, error: 'something went wrong' } })

      })

    }

  }


  static async  parseTitles(addresses) {

    const results = [];

    try {

      for (const url of addresses) {

        const response = await request.getWithPromise(url).catch(err => {
          results.push({ url, error: err.error ? err.error.code : (err.cause ? err.cause.code : err.message) })
        })

        response && results.push({ url, title: response.match(/<title[^>]*>([^<]+)<\/title>/)[1] })

      }

      return results

    } catch (error) {

      return results

    }

  }
  
}

module.exports = AsyncCtrl