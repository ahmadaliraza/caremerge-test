# Caremerge Test
this is implementation of test tasks

requires

- [Node.js](https://nodejs.org/dist/v8.11.4/node-v8.11.4.pkg) v8.9+
- [Node Package Manager (NPM)](https://www.npmjs.com/get-npm)

## Setup

install dependencies

```bash
$ npm install
```

## Start Server

all tasks are implemented and flow is decided in config.

change the `flow` property to `callback`, `promise` or `async`(default) in `config/default.json` file and start the server.

```bash
$ node app.js
```

server will be live and available on port `3000`. port can be changed from config file as well

navigate in browser to `http://localhost:<port>` to access the server

