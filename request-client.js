const request = require('request')
var rp = require('request-promise-native');
module.exports = {

  get: (url, callback) => {

    const headers = {
      "user-agent": 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:62.0) Gecko/20100101 Firefox/62.0'
    }

    request.get(url, { headers }, callback)

  },

  getWithPromise: (uri) => {

    const opts = {
      uri, headers: {
        "user-agent": 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:62.0) Gecko/20100101 Firefox/62.0'
      }
    }

    return rp.get(opts)
  }

}