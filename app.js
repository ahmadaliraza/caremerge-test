
const config = require('config');
const express = require('express');
const { asyncCtrl, callbackCtrl, promiseCtrl } = require('./controllers')

const app = express();
const http = require('http').Server(app);

app.set('json spaces', 2);
app.set('view engine', 'ejs');


app.get('/I/want/title/', (req, res) => {


  if (!req.query.address) {
    // if no title provided in query params
    return res.status(400).send('invalid address: please provide an address')
  }

  switch (config.get('flow')) {

    // get title through callbacks
    case "calback": callbackCtrl.getTile(req, res); break;

    // get title through promises
    case "promise": promiseCtrl.getTile(req, res); break;

    // get title through async
    case "async": asyncCtrl.getTile(req, res); break;

    //default: i like async/await :)
    default: asyncCtrl.getTile(req, res);

  }

})

app.use('*', (req, res) => {


  res.status(404).send('requested url not found')
})

/* Start the Server */
const server = http.listen(config.port, (err) => {

  if (err) {

    return console.log('ERR:: launching server ', err);

  }

  console.log(`server is live at ${config.host}:${config.port}`);

});

server.timeout = 4 * 60 * 1000;


process.on('uncaughtException', (err) => {

  console.log('Uncaught Exception thrown', err.stack);

});
process.on('unhandledRejection', (reason, p) => {

  console.log('Unhandled Rejection at: Promise', p, 'reason:', reason.stack);

});
